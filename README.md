# Docker Images

Repository to contain the build process for public docker images built by me and is intended for one-time, initial scaffolding of a new PHP projects and provides additional helpful Docker-focused functionality and is uniquely suited for containerized development.

## Commands

These are shortcut commands available with `make` inside each image directory:

- `run`: Run a container for testing purpose.
- `pull`: Pulls the image.
- `build`: Builds the image.
- `push`: (deprecated) Builds and pushes the image.

These are common arguments available:

Argument | Default             | Description
---------|--------------|-------------------
REPO     | $(notdir $(CURDIR)) | The repository name


## Customization

Here are some tips to customizing the images for each project's needs.